package org.example;

import java.text.Normalizer;
import java.util.Scanner;

public class Main {

    private static final char[] VOYELLES = {'a', 'e', 'i', 'o', 'u', 'y'};

    public static String stripAccents(String str) {
        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("[^a-zA-Z0-9]", "");
        return str;
    }

    public static int nombreVoyelles(String phrase) {
        int nombre = 0;
        for (char voyelle : VOYELLES) {
            for (int k = 0; k < phrase.length(); k++) {
                if (voyelle == phrase.charAt(k)) {
                    nombre++;
                }
            }
        }
        return nombre;
    }

    public static int compteurVoyelles(String phrase, char voyelle) {
        int nombre = 0;
        for (int k = 0; k < phrase.length(); k++) {
            if (voyelle == phrase.charAt(k)) {
                nombre++;
            }
        }
        return nombre;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String exemple;
        System.out.print("Veuillez saisir une phrase, nous allons compter vos voyelles ! -> ");
        exemple = stripAccents(input.nextLine().toLowerCase());

        System.out.println("Votre phrase est composée de " + nombreVoyelles(exemple) + " voyelles.");
        for (char chr : VOYELLES) {
            System.out.println("Votre phrase est composée de " + compteurVoyelles(exemple, chr) + " '" + chr + "'.");
        }
    }
}